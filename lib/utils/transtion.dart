import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

Future<dynamic> changeScreen(BuildContext context, Widget widget) async {
  await Navigator.push(
      context,
      PageTransition(
          type: PageTransitionType.fade,
          duration: Duration(milliseconds: 500),
          alignment: Alignment.bottomCenter,
          child: widget));
}

// request here
void changeScreenReplacement(BuildContext context, String pageName) {
  Navigator.of(context)
      .pushNamedAndRemoveUntil('/$pageName', (Route<dynamic> route) => false);
}