import 'package:flutter/material.dart';

const primary = Color(0xFFEC773F);
const green = Color(0xFF1A9AB1);
const white = Color(0xFFFFFFFF);

TextStyle styleFormField = TextStyle(fontWeight: FontWeight.w500);

InputDecoration decorationForm({ @required String libelle}){
  
  return InputDecoration(
    border: OutlineInputBorder(
      borderSide: BorderSide(color: primary ),
      borderRadius: BorderRadius.all(
        Radius.circular(10.0),
      ),
    ),
    filled: true,
    fillColor: Colors.white,
    // border: InputBorder.none,
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: primary ),
      borderRadius: BorderRadius.all(
        Radius.circular(10.0),
      ),
    ),
    contentPadding:
        EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
    hintText: libelle ,
    hintStyle: TextStyle(color: green )
  );

}