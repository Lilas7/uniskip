import 'package:flutter/material.dart';
import 'package:uniskip/utils/styles.dart';
import 'package:uniskip/utils/transtion.dart';
import 'package:uniskip/views/home.dart';

class IndexPage extends StatefulWidget {
  const IndexPage({ Key key }) : super(key: key);

  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  bool hideLoginForm = true;
  double opacity = 0.0;
  double h = 0;

  @override
  Widget build(BuildContext context) {

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: primary,
      body:  Stack(
        children: [

        Padding( 
          padding: EdgeInsets.all(20),
          child : Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [

            SizedBox(
                height: 150,
                child: Center(
                  child: Image.asset("assets/images/logo.png"),
                ),
            ),
            Container( 
              height : h , 
              child  : AnimatedOpacity(opacity: opacity , duration: Duration(seconds: 2) , child : loginView() )
            ),
            hideLoginForm ? Container(
              width: width - 200,
              child: RaisedButton(
                padding: EdgeInsets.all(20),
                textColor: white,  
                color: green,
                child:  Text("je me connecte".toUpperCase()),
                onPressed: () {
                  setState(() {
                    opacity = 1;
                    h = 140;
                    hideLoginForm = !hideLoginForm;
                  });
                },
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(10.0),
                ),
              )
            ) : Container(
              width: width - 200,
              child: RaisedButton(
                padding: EdgeInsets.all(20),
                textColor: white, 
                color: green,
                child:  Text("valider".toUpperCase()),
                onPressed: () => changeScreen(context, HomePage()),
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(10.0),
                ),
              )
            ),

            SizedBox(height: 10,),
            Container(
              width: width - 200,
              child: TextButton(
                child:  Text("Mot de passe oublié".toUpperCase(), style: TextStyle(color: Colors.white, decoration: TextDecoration.underline,),),
                onPressed: () =>  print(""),
              )
            ),
            SizedBox(height: 10,),
            Container(
              width: width - 200,
              child: RaisedButton(
                padding: EdgeInsets.all(20),
                textColor: green,
                color: white,
                child:  Text("je m'inscris".toUpperCase()),
                onPressed: () =>  print(""),
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(10.0),
                ),
              )
            ),

            
          ],
          )
        ),

        Positioned(
          top: 40,
          right: 30,
          child: IconButton(onPressed: (){}, icon: Icon(Icons.ac_unit, size: 50, color: white,))
        ),

        Positioned( 
          bottom: 0,
          right: 30,
          child: Text("v1.2020.12.14", style: TextStyle(color: white),)
        )

      ])
      ,
    );
  }


  loginView(){
    return Column(
      children: [
        TextFormField(
          cursorColor: Colors.black,
          keyboardType: TextInputType.emailAddress,
           style: const TextStyle(color: green ),
          decoration: decorationForm(libelle: "Adresse email"),
        ),
        SizedBox(height: 10,),
        TextFormField(
          cursorColor: Colors.black,
          keyboardType: TextInputType.emailAddress,
          style: const TextStyle(color: green ),
          obscureText: true,
          decoration: decorationForm(libelle: "Mot de passe") ,
        ),
        SizedBox(height: 20,),
      ],
    );
  }

}